package com.scoproject.androidsqlexmp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public ArrayList<ContactModel> contactModels = new ArrayList<>();
    AdapterContact adapterContact;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        adapterContact = new AdapterContact(getApplicationContext());
        final DatabaseHandler db = new DatabaseHandler(getApplicationContext());
        final EditText edUser = (EditText) findViewById(R.id.edUsername);
        final EditText edNoHp = (EditText) findViewById(R.id.edNoHp);
        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.viewpager);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        contactModels = MyApplication.getWritableDatabase().getAllContacts();
        adapterContact.setContactList(contactModels);
        recyclerView.setAdapter(adapterContact);

        Log.d("ukuran contact model", Integer.toString(adapterContact.getItemCount()));

        Log.d("Ukururan" , Integer.toString(contactModels.size()));
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                 db.addContact(new ContactModel(edUser.getText().toString(), edNoHp.getText().toString()));
                Toast.makeText(getApplicationContext(),"Data Berhasil Ditambahkan", Toast.LENGTH_SHORT)
                        .show();
                //refresh data
                Intent refresh = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(refresh);//Start the same Activity
                finish(); //finish Activity.
            }
        });


    }
}
